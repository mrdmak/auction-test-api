const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')
const { userService } = require('../services')
const CONTROLLER_NAME = 'AUTH_CONTROLLER'
const JWT_SECRET = process.env.JWT_SECRET

const login = async (req, res) => {
  const { email, password } = req.body

  try {
    const doc = await userService.getByEmail(email)

    const passwordMatch = await bcrypt.compare(password, doc.password)

    if (passwordMatch) {
      const token = jsonwebtoken.sign({
        email,
        exp: Math.floor(new Date().getTime() / 1000) + 24 * 60 * 60
      }, JWT_SECRET)
      res.json({ token })
    }
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Login error: `, err)
    res.sendStatus(500)
  }
}

const profile = async (req, res) => {
  const { email } = req.user

  try {
    const doc = await userService.getByEmail(email)

    res.json(doc)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting profile error: `, err)
    res.sendStatus(500)
  }
}

module.exports = {
  login,
  profile
}
