const { categoryService } = require('../services')
const CONTROLLER_NAME = 'CATEGORY_CONTROLLER'

const getAll = async (req, res) => {
  const { isAll } = req.query

  try {
    const docs = await categoryService.getAll(isAll)

    res.json(docs)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting all categories error: `, err)
    res.sendStatus(500)
  }
}

const getById = async (req, res) => {
  const { id } = req.params

  try {
    const doc = await categoryService.getById(id)

    res.json(doc)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting category by id error: `, err)
    res.sendStatus(500)
  }
}

const getBySlug = async (req, res) => {
  const { slug } = req.params

  try {
    const doc = await categoryService.getBySlug(slug)

    res.json(doc)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting category by slug error: `, err)
    res.sendStatus(500)
  }
}

const save = async (req, res) => {
  const category = req.body

  try {
    await categoryService.save(category)

    res.sendStatus(201)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Saving category error: `, err)
    res.sendStatus(500)
  }
}

const update = async (req, res) => {
  const category = req.body

  try {
    await categoryService.update(category)

    res.sendStatus(200)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Updating category error: `, err)
    res.sendStatus(500)
  }
}

const remove = async (req, res) => {
  const { id } = req.params

  try {
    await categoryService.remove(id)

    res.sendStatus(200)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Deleting category error: `, err)
    res.sendStatus(500)
  }
}

module.exports = {
  getAll,
  getById,
  getBySlug,
  save,
  update,
  remove
}
