const { userService } = require('../services')
const CONTROLLER_NAME = 'USER_CONTROLLER'

const getAll = async (req, res) => {
  try {
    const docs = await userService.getAll()

    res.json(docs)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting all users error: `, err)
    res.sendStatus(500)
  }
}

const getUserById = async (req, res) => {
  const { id } = req.params

  try {
    const doc = await userService.getById(id)

    res.json(doc)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting user by id error: `, err)
    res.sendStatus(500)
  }
}

const save = async (req, res) => {
  const user = req.body
  console.log('USER_DATA: ', user)
  try {
    await userService.save(user)

    res.sendStatus(201)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Saving user error: `, err)
    res.sendStatus(500)
  }
}

module.exports = {
  getAll,
  getUserById,
  save
}
