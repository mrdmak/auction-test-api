const { fileService } = require('../services')
const CONTROLLER_NAME = 'FILES_CONTROLLER'

const uploadImage = (req, res) => {
  if(req.file) {
    const imageUrl = fileService.uploadImage(req.file.filename)

    res.json({ imageUrl })
  } else {
    console.log(`[${CONTROLLER_NAME}] Uploading image error: `, err)
    res.sendStatus(500)
  }
}

const removeImage = async (req, res) => {
  const { imageUrl } = req.query

  try {
    await fileService.removeImage(imageUrl)

    res.sendStatus(200)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Removing image error: `, err)
    res.sendStatus(500)
  }
}

module.exports = {
  uploadImage,
  removeImage
}
