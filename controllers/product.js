const { productService } = require('../services')
const CONTROLLER_NAME = 'PRODUCT_CONTROLLER'

const getAll = async (req, res) => {
  try {
    const docs = await productService.getAll(req.query)

    res.json(docs)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting all products error: `, err)
    res.sendStatus(500)
  }
}

const getById = async (req, res) => {
  const { id } = req.params

  try {
    const doc = await productService.getById(id)

    res.json(doc)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting product by id error: `, err)
    res.sendStatus(500)
  }
}

const getBySlug = async (req, res) => {
  const { slug } = req.params

  try {
    const doc = await productService.getBySlug(slug)

    res.json(doc)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting product by slug error: `, err)
    res.sendStatus(500)
  }
}

const getMinMaxPricing = async (req, res) => {
  const { categorySlug } = req.query

  try {
    const minMax = await productService.getMinMaxPricing(categorySlug)

    res.json(minMax)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Getting product by slug error: `, err)
    res.sendStatus(500)
  }
}

const save = async (req, res) => {
  const product = req.body

  try {
    await productService.save(product)

    res.sendStatus(201)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Saving product error: `, err)
    res.sendStatus(500)
  }
}

const update = async (req, res) => {
  const product = req.body

  try {
    await productService.update(product)

    res.sendStatus(200)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Updating product error: `, err)
    res.sendStatus(500)
  }
}

const remove = async (req, res) => {
  const { id } = req.params

  try {
    await productService.remove(id)

    res.sendStatus(200)
  } catch (err) {
    console.log(`[${CONTROLLER_NAME}] Deleting product error: `, err)
    res.sendStatus(500)
  }
}

module.exports = {
  getAll,
  getById,
  getBySlug,
  getMinMaxPricing,
  save,
  update,
  remove
}
