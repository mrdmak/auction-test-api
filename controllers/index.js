const authController = require('./auth')
const userController = require('./user')
const categoryController = require('./category')
const productController = require('./product')
const filesController = require('./files')

module.exports = {
  authController,
  userController,
  categoryController,
  productController,
  filesController
}
