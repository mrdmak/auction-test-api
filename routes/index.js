const express = require('express')
const multer = require('multer')
const {
  authController,
  userController,
  categoryController,
  productController,
  filesController
} = require('../controllers')

const router = express.Router()

// Auth routes
router.post('/auth/login', authController.login)
router.get('/auth/profile', authController.profile)

// User routes
router.get('/users', userController.getAll)
router.get('/users/:id', userController.getUserById)
router.post('/users', userController.save)

// Category routes
router.get('/categories', categoryController.getAll)
router.get('/categories/:id', categoryController.getById)
router.get('/categories/slug/:slug', categoryController.getBySlug)
router.post('/categories', categoryController.save)
router.put('/categories', categoryController.update)
router.delete('/categories/:id', categoryController.remove)

// Product routes
router.get('/products', productController.getAll)
router.get('/products/:id', productController.getById)
router.get('/products/slug/:slug', productController.getBySlug)
router.get('/products/minmax/pricing', productController.getMinMaxPricing)
router.post('/products', productController.save)
router.put('/products', productController.update)
router.delete('/products/:id', productController.remove)

// Upload images
const upload = multer({ dest: __dirname + '../../public/uploads/images' })
router.post('/upload', upload.single('image'), filesController.uploadImage)
router.delete('/remove-image', filesController.removeImage)

module.exports = router
