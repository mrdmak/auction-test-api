const mongoose = require('mongoose')
const slug = require('mongoose-slug-updater')

// Adding slug plugin
mongoose.plugin(slug)

const Schema = mongoose.Schema

const categorySchema = new Schema({
  translations: [{
    title: {
      type: String,
      required: true
    },
    slug: {
      type: String,
      slug: 'title'
    },
    locale: {
      type: String,
      required: true
    }
  }],
  active: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
})

const Category = mongoose.model('Category', categorySchema)

module.exports = Category
