const mongoose = require('mongoose')

const Schema = mongoose.Schema

const productSchema = new Schema({
  translations: [{
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    slug: {
      type: String,
      slug: 'title'
    },
    locale: {
      type: String,
      required: true
    }
  }],
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  pricing: {
    type: Number
  },
  imageUrl: {
    type: String
  },
  active: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  }
})

const Product = mongoose.model('Product', productSchema)

module.exports = Product
