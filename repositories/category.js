const { Category } = require('../models')
const REPOSITORY_NAME = 'CATEGORY_REPOSITORY'

const getAll = async (criteria) => {
  try {
    const docs = await Category.find(criteria)

    return docs
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting all categories error: `, err)
  }
}

const getById = async (id) => {
  try {
    const doc = await Category.findById(id)

    return doc
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting category by id error: `, err)
  }
}

const getBySlug = async (slug) => {
  try {
    const doc = await Category.findOne({ 'translations': { $elemMatch: { slug } } })

    return doc
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting category by slug error: `, err)
  }
}

const save = async (category) => {
  try {
    await Category.create(category)
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Saving category error: `, err)
  }
}

const update = async (category) => {
  try {
    await Category.findByIdAndUpdate(category._id, category)
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Updating category error: `, err)
  }
}

const remove = async (id) => {
  try {
    await Category.findByIdAndDelete(id)
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Deleting category error: `, err)
  }
}

module.exports = {
  getAll,
  getById,
  getBySlug,
  save,
  update,
  remove
}