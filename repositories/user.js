const { User } = require('../models')
const REPOSITORY_NAME = 'USER_REPOSITORY'

const getAll = async () => {
  try {
    const docs = await User.find()

    return docs
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting all users error: `, err)
  }
}

const getById = async (id) => {
  try {
    const doc = await User.findById(id)

    return doc
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting user by id error: `, err)
  }
}

const getByEmail = async (email) => {
  try {
    const doc = await User.findOne({ email })

    return doc
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting user by email error: `, err)
  }
}

const save = async (user) => {
  try {
    await User.create(user)
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Saving user error: `, err)
  }
}

module.exports = {
  getAll,
  getById,
  getByEmail,
  save
}
