const userRepository = require('./user')
const categoryRepository = require('./category')
const productRepository = require('./product')

module.exports = {
  userRepository,
  categoryRepository,
  productRepository
}
