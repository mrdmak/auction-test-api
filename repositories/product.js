const { Product } = require('../models')
const REPOSITORY_NAME = 'PRODUCT_REPOSITORY'

const getAll = async (criteria) => {
  try {
    const docs = await Product.find(criteria)

    return docs
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting all products error: `, err)
  }
}

const getById = async (id) => {
  try {
    const doc = await Product.findById(id)

    return doc
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting product by id error: `, err)
  }
}

const getBySlug = async (slug) => {
  try {
    const doc = await Product.findOne({ 'translations': { $elemMatch: { slug } } })

    return doc
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting product by slug error: `, err)
  }
}

const getMinMaxPricing = async (criteria) => {
  try {
    const min = await Product.findOne(criteria).sort({ pricing: 1 })
    const max = await Product.findOne(criteria).sort({ pricing: -1 })

    return [min, max]
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Getting product by min and max pricing error: `, err)
  }
}

const save = async (product) => {
  try {
    await Product.create(product)
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Saving product error: `, err)
  }
}

const update = async (product) => {
  try {
    await Product.findByIdAndUpdate(product._id, product)
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Updating product error: `, err)
  }
}

const remove = async (id) => {
  try {
    await Product.findByIdAndDelete(id)
  } catch (err) {
    console.log(`[${REPOSITORY_NAME}] Deleting product error: `, err)
  }
}

module.exports = {
  getAll,
  getById,
  getBySlug,
  getMinMaxPricing,
  save,
  update,
  remove
}
