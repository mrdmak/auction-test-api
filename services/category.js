const { categoryRepository } = require('../repositories')
const SERVICE_NAME = 'CATEGORY_SERVICE'

const getAll = async (isAll) => {
  const criteria = isAll === 'true' ? {} : { 'active': true }

  try {
    const docs = await categoryRepository.getAll(criteria)

    return docs
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting all categories error: `, err)
  }
}

const getById = async (id) => {
  try {
    const doc = await categoryRepository.getById(id)

    return doc
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting category by id error: `, err)
  }
}

const getBySlug = async (slug) => {
  try {
    const doc = await categoryRepository.getBySlug(slug)

    return doc
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting category by slug error: `, err)
  }
}

const save = async (category) => {
  try {
    await categoryRepository.save(category)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Saving category error: `, err)
  }
}

const update = async (category) => {
  try {
    await categoryRepository.update(category)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Updating category error: `, err)
  }
}

const remove = async (id) => {
  try {
    await categoryRepository.remove(id)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Deleting category error: `, err)
  }
}

module.exports = {
  getAll,
  getById,
  getBySlug,
  save,
  update,
  remove
}
