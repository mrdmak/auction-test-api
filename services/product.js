const { productRepository, categoryRepository } = require('../repositories')
const fileService = require('./files')
const SERVICE_NAME = 'PRODUCT_SERVICE'

const getAll = async (query) => {
  let criteria

  switch (true) {
    case query.isAll === 'true':
      criteria = {}
      break

    case !isNaN(query.gte) && !isNaN(query.lte) && query.categoryId !== 'null':
      criteria = {
        'category': query.categoryId,
        'pricing': { $gte: query.gte, $lte: query.lte } 
      }
      break

    default:
      criteria = { 'active': true }
      break
  }

  try {
    const docs = await productRepository.getAll(criteria)

    return docs
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting all products error: `, err)
  }
}

const getById = async (id) => {
  try {
    const doc = await productRepository.getById(id)

    return doc
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting product by id error: `, err)
  }
}

const getBySlug = async (slug) => {
  try {
    const doc = await productRepository.getBySlug(slug)

    return doc
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting product by slug error: `, err)
  }
}

const getMinMaxPricing = async (categorySlug) => {
  try {
    const category = await categoryRepository.getBySlug(categorySlug)
    const criteria = {
      'active': true,
      'category': category._id
    }

    const minMax = await productRepository.getMinMaxPricing(criteria)

    const result = minMax
      .map((item) => {
        if (!item) {
          return 0
        }

        return item.pricing
      })

    return result
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting product by slug error: `, err)
  }
}

const save = async (product) => {
  try {
    await productRepository.save(product)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Saving product error: `, err)
  }
}

const update = async (product) => {
  try {
    await productRepository.update(product)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Updating product error: `, err)
  }
}

const remove = async (id) => {
  try {
    const doc = await productRepository.getById(id)

    if (doc.imageUrl.length > 0) {
      await fileService.removeImage(doc.imageUrl)
    }

    await productRepository.remove(id)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Deleting product error: `, err)
  }
}

module.exports = {
  getAll,
  getById,
  getBySlug,
  getMinMaxPricing,
  save,
  update,
  remove
}
