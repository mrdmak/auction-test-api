const fs = require('fs')
const path = require('path')
const util = require('util')
const unlink = util.promisify(fs.unlink)
const SERVICE_NAME = 'FILE_SERVICE'

const uploadImage = (filename) => {
  const imageUrl = `/uploads/images/${filename}`

  return imageUrl
}

const removeImage = async (imageUrl) => {
  try {
    const fullPath = path.resolve(__dirname + `../../public${imageUrl}`)

    await unlink(fullPath)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Deleting file error: `, err)
  }
}

module.exports = {
  uploadImage,
  removeImage
}
