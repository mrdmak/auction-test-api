const userService = require('./user')
const categoryService = require('./category')
const productService = require('./product')
const fileService = require('./files')

module.exports = {
  userService,
  categoryService,
  productService,
  fileService
}
