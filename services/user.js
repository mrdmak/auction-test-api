const bcrypt = require('bcrypt')
const { userRepository } = require('../repositories')
const SERVICE_NAME = 'USER_SERVICE'

const getAll = async () => {
  try {
    const docs = await userRepository.getAll()

    return docs
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting all users error: `, err)
  }
}

const getById = async (id) => {
  try {
    const doc = await userRepository.getById(id)

    return doc
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting user by id error: `, err)
  }
}

const getByEmail = async (email) => {
  try {
    const doc = await userRepository.getByEmail(email)

    return doc
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Getting user by email error: `, err)
  }
}

const save = async (user) => {
  const plainPassword = user.password
  const saltRounds = 10

  try {
    const hashedPassword = await bcrypt.hash(plainPassword, saltRounds)

    const userWithHashedPassword = {
      ...user,
      password: hashedPassword
    }

    await userRepository.save(userWithHashedPassword)
  } catch (err) {
    console.log(`[${SERVICE_NAME}] Saving user error: `, err)
  }
}

module.exports = {
  getAll,
  getById,
  getByEmail,
  save
}
