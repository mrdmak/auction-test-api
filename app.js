require('dotenv').config()
const http = require('http')
const express = require('express')
const jwt = require('express-jwt')
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const router = require('./routes')

// Constants
const PORT = process.env.PORT || 8000
const JWT_SECRET = process.env.JWT_SECRET
const DB_URI = process.env.DB_URI
const DB_NAME = process.env.DB_NAME
const DB_USER = process.env.DB_USER
const DB_PASSWORD = process.env.DB_PASSWORD

// Mongoose connect
mongoose.connect(
  `mongodb+srv://${DB_USER}:${DB_PASSWORD}@${DB_URI}/${DB_NAME}?retryWrites=true&w=majority`,
  { 
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  }
)

// Express app instance
const app = express()

// Static files
app.use(express.static('public'))

// Express app middleware
app.use(jwt({ secret: JWT_SECRET }).unless({
  path: [
    { url: /^\/uploads\/images\/.*/, method: 'GET' },
    { url: '/api/v1/auth/login', method: 'POST' },
    { url: '/api/v1/categories', method: 'GET' },
    { url: /^\/api\/v1\/categories\/slug\/.*/, method: 'GET' },
    { url: '/api/v1/products', method: 'GET' },
    { url: /^\/api\/v1\/products\/slug\/.*/, method: 'GET' },
    { url: '/api/v1/products/minmax/pricing', method: 'GET' }
  ]
}))
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('invalid token...');
  }
})
app.use(bodyParser.json())
app.use(cors())

// Include router
app.use('/api/v1', router)

http.createServer(app).listen(PORT)
